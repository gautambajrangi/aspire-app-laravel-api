
1.Install Passport & Configure .env file & Make QUEUE_CONNECTION=database for job run purpose in .env

2.Import Postman Api Collection from "documentation" folder

3.Login or Register with all Required Details, I am added needed data to apply for a loan

4.Call api "loan/check-eligibility" to check loan eligibility

5.Apply for loan or check loan breakpoints with Api "loan/apply-for-loan" with desired amount

6.Loan will processed & through Cron, I am deducting amount from users table wallet_balance column [for test].

Thanks