<?php

return [

    'SITE_TITLE' => "AspireApp",
    'emptyData' => new \stdClass(),
    'isApiLogEnabled' => true,
    'currentMinSalaryToApplyForLoan' => 9999,

    'repaymentFrequencyType' => ["M", "W"],

    'per_page' => 10,
    'per_page_large' => 50,

    'is_active'    => [
        'true'    => 1,
        'false' => 0,
    ],
    
    'is_editable'    => [
        'true'    => 1,
        'false' => 0,
    ],

    'is_confirmed'     => [ 
        'true' => 1,
        'false' => 0, 
    ],

    'dirName'     => [ 
        'uploads' => "uploads",
    ],

 /*-----------*/ 
'apiErrorCode'     => [ 
    'EMAIL_NOT_VERIFIED' => [ "shortkey" => "EMAIL_NOT_VERIFIED", "name" => "EMAIL NOT VERIFIED", "name2" => "USER EMAIL IS NOT VERIFIED" ],
    'INVALID_CREDENTIAL' => [ "shortkey" => "INVALID_CREDENTIAL", "name" => "INVALID CREDENTIAL", "name2" => "INVALID CREDENTIAL" ],
    'USER_NOT_FOUND' => [ "shortkey" => "USER_NOT_FOUND", "name" => "USER NOT FOUND", "name2" => "USER NOT REGISTER" ],
    'SERVER_ERROR' => [ "shortkey" => "SERVER_ERROR", "name" => "SERVER ERROR", "name2" => "SERVER ERROR" ],
    'MESSAGE_DISPLAY' => [ "shortkey" => "MESSAGE_DISPLAY", "name" => "MESSAGE DISPLAY", "name2" => "MESSAGE DISPLAY" ],
],
/*-------------*/
];





