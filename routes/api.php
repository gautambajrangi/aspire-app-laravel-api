<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\LoanController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/



  Route::post('login', [AuthController::class,'login'])->name('login');
  Route::post('register', [AuthController::class,'signup']);




  Route::group(['middleware' => ['auth:api']], function (){


    Route::prefix('loan')->group(function () {
      Route::post('check-eligibility', [LoanController::class, 'checkLoanEligibility']);
      Route::post('apply-for-loan', [LoanController::class, 'applyForLoan']);
      Route::post('listing', [LoanController::class, 'listing']);
      
    });




  	Route::prefix('user')->group(function () {
  		Route::get('profile', [AuthController::class,'getProfile']);
  		Route::patch('update', [AuthController::class,'updateProfile']);
      Route::match(array('GET','POST'), 'logout', [AuthController::class,'logout']);
  	});

	  
      
	  
	   
	   
	   
  });




