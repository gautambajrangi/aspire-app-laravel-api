<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\LoanType;

class LoanTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        LoanType::truncate();
        LoanType::insert([
        	["name" => "Home Renovation", "loan_percent" => 5.65 ],
        	["name" => "Wedding", "loan_percent" => 8 ],
        	["name" => "Medical", "loan_percent" => 1 ],
            ["name" => "Personal", "loan_percent" => 9.25 ],
        ]);
    }
}
