<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EmployementType;

class EmployementTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        EmployementType::truncate();
        EmployementType::insert([
        	["name" => "Salaried"],
        	["name" => "Self Employed"],
        	["name" => "Business"]
        ]);
    }
}
