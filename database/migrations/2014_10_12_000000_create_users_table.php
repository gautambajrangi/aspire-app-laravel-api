<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('firstname', 80);
            $table->string('lastname', 80);
            $table->string('middlename', 80);
            $table->string('email');
            $table->string('mobile_number', 15)->nullable()->default(NULL);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('is_active')->default(1)->comment('1-active|0-deactive|2-deleted');
            $table->timestamps();
            $table->integer('employement_type');
            $table->integer('total_monthly_income');
            $table->date('dob');
            $table->char('pan_number', 10)->comment('government id');
            $table->boolean('is_eligible_forloan')->default(0)->comment('0-false|1-true');
            $table->string('address');
            $table->string('city', 100);
            $table->string('zipcode', 6);
            $table->string('country', 100);
            $table->string('country_code', 3); // like US, SG, IN etc
            $table->decimal('wallet_balance', $precision = 11, $scale = 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
