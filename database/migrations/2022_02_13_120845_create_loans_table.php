<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->integer('loan_type_id');
            $table->string('loan_number', 25)->comment('Loan id number random uniqid');
            $table->decimal('amount', $precision = 9, $scale = 2)->comment('total loan amount');
            $table->decimal('loan_percent', $precision = 3, $scale = 2); 
            $table->timestamps();
            $table->boolean('is_active')->default(1)->comment('1-active|0-closed');
            $table->decimal('processing_fee', $precision = 8, $scale = 2)->default(0);
            $table->decimal('grand_amount', $precision = 11, $scale = 2);
            $table->decimal('tax_amount', $precision = 9, $scale = 2);
            $table->smallInteger('emi_count')->default(1);
            $table->char('repayment_frequency', 2)->default("M")->comment("M-monthly|W-Weekly");
            $table->decimal('penalty_amount_total', $precision = 9, $scale = 2)->default(0);
            $table->decimal('collected_amount_total', $precision = 11, $scale = 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
