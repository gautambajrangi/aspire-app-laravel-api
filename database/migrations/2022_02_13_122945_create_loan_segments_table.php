<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanSegmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_segments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('loan_id');
            $table->decimal('emi_amount', $precision = 8, $scale = 2);
            $table->decimal('loan_percent', $precision = 3, $scale = 2);
            $table->smallInteger('emi_number');
            $table->timestamps();
            $table->boolean('is_active')->default(1)->comment('1-paid|0-unpaid');
            $table->date('start_date');
            $table->date('end_date');
            $table->decimal('penalty_amount', $precision = 6, $scale = 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_segments');
    }
}
