<?php

namespace App\Listeners;

use App\Events\LoanProcessed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\User;
use App\Mail\LoanProcessedMail;

class SendLoanProcessedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\LoanProcessed  $event
     * @return void
     */
    public function handle(LoanProcessed $event)
    {
        $first = User::find($event->loan['user_id']);
        \Mail::to($first->email)->send(new LoanProcessedMail($event));
    }
}
