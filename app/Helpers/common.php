<?php

use App\Models\ApiLog;

function randomString($length_of_string = 8) {
    $chr = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
    $randomString = ''; 
  
    for ($i = 0; $i < $length_of_string; $i++) { 
        $index = rand(0, strlen($chr) - 1); 
        $randomString .= $chr[$index]; 
    }   
    return $randomString; 
}

function dde($arr){
    echo "<pre>"; print_r($arr); die;
}

function CreateApiLogs($dataApi=[]){
    try {
        if(Config::get('constants.isApiLogEnabled')==true){
            $e = new ApiLog;
            $e->data = json_encode($dataApi);
            $e->save();
            $from = date('Y-m-d', strtotime(date('Y-m-d') . ' -30 day'));
            $to = date('Y-m-d', strtotime(date('Y-m-d') . ' +1 day'));
            ApiLog::whereNotBetween('created_at', [$from, $to])->delete();
        }
    } catch(\Exception $e) {  }
}

function UploadImage($file, $dir,$filename_prefix='',$fileName='') {
    if($fileName==''){
        $fileName = strtolower($filename_prefix.uniqid().mt_rand(111,999999). '.' . $file->getClientOriginalExtension());
    }
    else
    {
       $fileName = strtolower($filename_prefix. $fileName. '.' .$file->getClientOriginalExtension()); 
    }
    Storage::disk('public')->putFileAs($dir, $file, $fileName);
    //$file->move(public_path('storage'. '/'. $dir), $fileName);
    //$urlFile = public_path('/storage') . '/' . $dir . '/' . $fileName;
    return $fileName;
}

function DeleteFile($filename, $dir) {
    $existImage = storage_path() . '/app/public/' . $dir . '/' . $filename;
        if (File::exists($existImage)) {
            File::delete($existImage);
        }
    return true;
}

function getConst($key=''){
    if(trim($key=='')){
        return NULL;
    }
    else
    {
        return Config::get('constants.'.$key);
    }
}

function sendPath($dir=''){
    return asset('storage').'/'.$dir.'/';
}

function RemoveSpecialChar($str) {
    $res = str_replace(array( '\'', '"', ',' , ';', '<', '>' ), '', $str);
    return $res;
}

function stringToSlug($string){
    $slug = preg_replace('/[^a-zA-Z0-9 -]/','', trim($string));
    $slug = str_replace(' ','-', $slug);
    return strtolower($slug);
}

function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');
    $realEnd = new DateTime($end);
    $realEnd->add($interval);
    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
    foreach($period as $date) {                 
        $array[] = $date->format($format); 
    }
    return $array;
}

function dateDiff($date1, $date2) {
    return round((strtotime($date2) - strtotime($date1)) / 86400);
}


