<?php

namespace App\Traits;
use Illuminate\Database\Eloquent\Builder;
use App\Models\User;
use App\Models\LoanType;

trait LoanTrait
{

    public function getEligibileLoanAmount($user_id) {
        $is_eligibile = $amountEligibileMax = 0; 

        $first = User::where('id', $user_id)
        ->where('is_active', getConst('is_active.true'))
        ->first(['id', 'total_monthly_income', 'is_eligible_forloan', 'total_monthly_income']);

        $loans_count = isset($first->current_active_loan_count) ? $first->current_active_loan_count : 0;

        if(isset($first->id) && $first->is_eligible_forloan==1 && $first->total_monthly_income>getConst('currentMinSalaryToApplyForLoan') && $loans_count < 2){
            $is_eligibile = 1;
            $amountEligibileMax = round($first->total_monthly_income*(8- $loans_count*3));
            if($amountEligibileMax <= getConst('currentMinSalaryToApplyForLoan')){
                $is_eligibile = 0;
            }
        }
        $data = ["is_eligibile" => $is_eligibile, "amountEligibileMax" => $amountEligibileMax];
        return $data;
    }

    public function getLoanBreakpoints($applied_loan_amount, $emi_count=3, $repaymentFrequencyType="W", $loan_type_id) {
        $totalLoanAmount = $applied_loan_amount;
        $arrayLoanBreakpoints = [];

        $firstLoanType = LoanType::find($loan_type_id);
        $processing_fees = $firstLoanType->loan_percent * $applied_loan_amount * 0.01;
        $tax_fees = 2 * $applied_loan_amount * 0.01; // 2% tax applied test only

        $total_amount = $applied_loan_amount - $tax_fees - $processing_fees;

        for ($i=0; $i < $emi_count; $i++) {
            if($i+1 != $emi_count){
            $arrayLoanBreakpoints[$i]['emiAmount'] = round($applied_loan_amount/$emi_count);
            $arrayLoanBreakpoints[$i]['emiNumber'] = $i+1;
            $totalLoanAmount -= $arrayLoanBreakpoints[$i]['emiAmount'];
            } else {
            $arrayLoanBreakpoints[$i]['emiAmount'] = $totalLoanAmount;
            $arrayLoanBreakpoints[$i]['emiNumber'] = $i+1;
            $totalLoanAmount -= $totalLoanAmount;
            }
        }

        $data = [ 'loanBreakpoints' => $arrayLoanBreakpoints, 'processing_fees' => $processing_fees, 'tax_fees' => $tax_fees, 'loan_percent' => $firstLoanType->loan_percent, 'total_amount' => $total_amount ];
        return $data;
    }







}
