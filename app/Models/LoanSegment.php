<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanSegment extends Model
{
    use HasFactory;

    protected $fillable = [
        'loan_id',
        'emi_amount',
        'emi_number',
        'is_active',
        'start_date',
        'end_date',
        'penalty_amount',
        'loan_percent',
    ];

    public function loan(){
        return $this->hasOne(Loan::class, 'id', 'loan_id');
    }



    
}
