<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Loan extends Model
{
    use HasFactory;

    protected $table  = 'loans';
    public $timestamps = true;
    protected $primaryKey = 'id';
    public $incrementing = true;

    protected static function boot() {
        parent::boot();
        static::creating(function (Model $model) {
            $model->loan_number = Str::upper(Str::random(25));
        });
    }

    protected $fillable = [
        'user_id',
        'loan_type_id',
        'loan_number',
        'amount',
        'is_active',
        'processing_fee',
        'grand_amount',
        'tax_amount',
        'emi_count',
        'repayment_frequency',
        'penalty_amount_total',
        'collected_amount_total',
        'loan_percent'
    ];

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function loan_segments(){
        return $this->hasMany(LoanSegment::class, 'loan_id', 'id');
    }

    public function loantype(){
        return $this->hasOne(LoanType::class, 'id', 'loan_type_id');
    }





}
