<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
//use Laravel\Sanctum\HasApiTokens;
use Laravel\Passport\HasApiTokens;
use App\Models\Loan;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'firstname',
        'lastname',
        'middlename',
        'mobile_number',
        'email',
        'password',
        'is_active',
        'employement_type',
        'total_monthly_income',
        'dob',
        'pan_number',
        'is_eligible_forloan',
        'address',
        'city',
        'zipcode',
        'country',
        'country_code',
        'wallet_balance', // this is created to charge emi amount directly from it & avoiding all bank details for testing

    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'updated_at'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'current_active_loan_count'
    ];
    public function getCurrentActiveLoanCountAttribute(){   
        return Loan::where('user_id', $this->id)->where('is_active', getConst('is_active.true'))->count();
    }

    public function loans(){
        return $this->hasMany(Loan::class, 'user_id', 'id');
    }



}
