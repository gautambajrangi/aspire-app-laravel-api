<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Auth;
use App\Models\User;
use Illuminate\Validation\Rule;
// use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\Rules\Password;
use Carbon\Carbon;



class AuthController extends Controller
{

    public function login(Request $request){
        try {
        $validator = Validator::make($request->all(), [ 
            'email' => 'required|email|string',
            'password' => 'required|string|min:6'
        ]);

        if($validator->fails()) {  
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
                'data'    => getConst('emptyData'),
                'show' => true
            ], 422);   
        }

        $first = User::where('email', trim($request->email))->where('is_active', getConst('is_active.true'))->first();

        if(isset($first->password)){
            if (password_verify($request->password, $first->password)) 
            {
                $accessToken = $first->createToken('AuthToken')->accessToken;
                $data = [ 'user' => $first, 'access_token' => $accessToken];
                
                return response()->json([
                    'success' => true,
                    'message' => "Logged In Successfully.",
                    'data'    => $data,
                    'show' => true
                ], 200);
            }
            else
            {
                return response()->json([
                    'success' => false,
                    'message' => 'This Email or Password is Incorrect!',
                    'data'    => getConst('emptyData'),
                    'show' => true
                ], 200);
            }
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => 'Email Not Found!',
                'data'    => getConst('emptyData'),
                'show' => true
            ], 200);
        }
        } catch (\Exception $e) {
            CreateApiLogs($e->getMessage());
            Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => "Error! Something Went Wrong.",
                'data'    => getConst('emptyData'),
                'show' => true,
                'errorInfo' => $e->getMessage(),
                'error_code' => getConst('apiErrorCode.SERVER_ERROR.shortkey'),
            ], 400); // not showing 500 status code
        }
    }

    /*------
    Registration New Account
    Not adding too much details for loan eligibility
    ------*/
    public function signup(Request $request) {
        try {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'firstname' => 'required|string|max:80',
            'lastname' => 'required|string|max:80',
            'middlename' => 'required|string|max:80',
            'password' => ['required', 'confirmed', 'string', Password::min(6)->letters()->mixedCase()->numbers() ],
            //password_confirmation
            'employement_type' => 'required|integer|exists:App\Models\EmployementType,id',
            'total_monthly_income' => 'required|integer|min:0|max:999999999',
            'date_of_birth' => 'required|date_format:Y-m-d|before_or_equal:'.Carbon::now()->subYears(18)->format('Y-m-d'),
            'pan_number' => 'required|string|max:10|min:10',
            'address' => 'required|string|max:255',
            'city' => 'required|string|max:100',
            'zipcode' => 'required|max:6',
        ]); 
        if($validator->fails()) {  
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
                'data'    => getConst('emptyData'),
                'show' => true
            ], 422);   
        }

        $one = User::where('email', trim($request->email))->first(['id']);
        if(isset($one->id)){
            return response()->json([
                'success' => false,
                'message' => "Please Check, This User is Already Registered With Us.",
                'data'    => getConst('emptyData'),
                'show' => true
            ], 200);
        }
        else
        {
            $fullname = trim($request->firstname)."".trim($request->middlename)."".trim($request->lastname);
            $dataInsert = [
                "name" => $fullname,
                "firstname" => trim($request->firstname),
                "middlename" => trim($request->middlename),
                "lastname" => trim($request->lastname),
                "email" => trim($request->email),
                "email_verified_at" => date('Y-m-d H:i:s'),
                "employement_type" => $request->employement_type,
                "total_monthly_income" => $request->total_monthly_income,
                "dob" => $request->date_of_birth,
                "pan_number" => trim($request->pan_number),
                "password" => bcrypt($request->password),
                "is_active" => getConst('is_active.true'),
                "address" => trim($request->address),
                "zipcode" => trim($request->zipcode),
                "city" => trim($request->city),
                "country" => "India",
                "country_code" => "IN",
                "is_eligible_forloan" => ($request->total_monthly_income>getConst('currentMinSalaryToApplyForLoan')) ? 1 : 0,
            ];
            $lastCreated = User::create($dataInsert);

            if(isset($lastCreated->id)){
                return response()->json([
                    'success' => true,
                    'message' => "You are Registered Successfully.",
                    'data'    => [
                        'first' => $lastCreated,
                    ],
                    'show' => true
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Something Went Wrong.",
                    'data'    => getConst('emptyData'),
                    'show' => true,
                ], 200);
            }
        }
        } catch (\Exception $e) {
            CreateApiLogs($e->getMessage());
            Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => "Error! Something Went Wrong.",
                'data'    => getConst('emptyData'),
                'show' => true,
                'errorInfo' => $e->getMessage(),
                'error_code' => getConst('apiErrorCode.SERVER_ERROR.shortkey'),
            ], 400); // not showing 500 status code
        }
    }

    public function logout() {
        try {
        if(Auth::check()) {
            Auth::user()->token()->revoke();
            return response()->json([
                'success' => true,
                'message' => 'Log Out Successfully.',
                'data'    => getConst('emptyData'),
                'show' => false
            ], 200);
        }
        
        return response()->json([
            'success' => true,
            'message' => 'Failed to Log Out.',
            'data'    => getConst('emptyData'),
            'show' => false
        ], 401);

        } catch (\Exception $e) {
            CreateApiLogs($e->getMessage());
            Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => "Error! Something Went Wrong.",
                'data'    => getConst('emptyData'),
                'show' => true,
                'errorInfo' => $e->getMessage(),
                'error_code' => getConst('apiErrorCode.SERVER_ERROR.shortkey'),
            ], 400); // not showing 500 status code
        }
    }

    public function getProfile() {
        try {
        if(Auth::check()) {
            $user = Auth::user();
            $first = User::where('id', trim($user->id))->where('is_active', getConst('is_active.true'))->first();
            if(isset($first->id)){
                return response()->json([
                    'success' => true,
                    'message' => '',
                    'data'    => [
                        'first' => $first,
                    ],
                    'show' => false
                ], 200);
            }
        }
        
        return response()->json([
            'success' => true,
            'message' => 'User Not Found.',
            'data'    => getConst('emptyData'),
            'show' => false
        ], 401);

        } catch (\Exception $e) {
            CreateApiLogs($e->getMessage());
            Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => "Error! Something Went Wrong.",
                'data'    => getConst('emptyData'),
                'show' => true,
                'errorInfo' => $e->getMessage(),
                'error_code' => getConst('apiErrorCode.SERVER_ERROR.shortkey'),
            ], 400);
        }
    }

    public function updateProfile(Request $request) {
        try {
        if(Auth::check()) {
        $user = Auth::user();
        $first = User::where('id', trim($user->id))->where('is_active', getConst('is_active.true'))->first();
        if(isset($first->id)){

            $validator = Validator::make($request->all(), [
                'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($first->id),],
                'mobile_number' => ['nullable', 'string', 'max:15', Rule::unique('users')->ignore($first->id),],
                'firstname' => 'required|string|max:80',
                'lastname' => 'required|string|max:80',
                'middlename' => 'required|string|max:80',
            ]); 
            if($validator->fails()) {  
                return response()->json([
                    'success' => false,
                    'message' => $validator->errors()->first(),
                    'data'    => getConst('emptyData'),
                    'show' => true
                ], 200);   
            }

            $fullname = trim($request->firstname)."".trim($request->middlename)."".trim($request->lastname);
            $dataUpdate = [
                "email" => trim($request->email),
                "name" => $fullname,
                "firstname" => trim($request->firstname),
                "middlename" => trim($request->middlename),
                "lastname" => trim($request->lastname),
                "mobile_number" => isset($request->mobile_number) ? trim($request->mobile_number) : NULL,
            ];
            User::where('id', $first->id)->update($dataUpdate);

            return response()->json([
                'success' => true,
                'message' => "User Updated Successfully.",
                'data'    => [
                    'first' => $first->fresh(),
                ],
                'show' => true
            ], 200);
        }
        }
        return response()->json([
            'success' => true,
            'message' => 'User Not Found.',
            'data'    => getConst('emptyData'),
            'show' => false
        ], 401);

        } catch (\Exception $e) {
            CreateApiLogs($e->getMessage());
            Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => "Error! Something Went Wrong.",
                'data'    => getConst('emptyData'),
                'show' => true,
                'errorInfo' => $e->getMessage(),
                'error_code' => getConst('apiErrorCode.SERVER_ERROR.shortkey'),
            ], 400);
        }
    }

    








}
