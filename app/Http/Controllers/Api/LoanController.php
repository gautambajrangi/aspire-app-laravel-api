<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Loan;
use App\Models\LoanSegment;
use App\Models\LoanType;
use App\Traits\LoanTrait;
use App\Events\LoanProcessed;

class LoanController extends Controller
{
    use LoanTrait;

    public function checkLoanEligibility(Request $request) {
        try {

        $validator = Validator::make($request->all(), [
            'current_monthly_salary' => 'required|integer|min:0|max:999999999',
        ]);
        if($validator->fails()) {  
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
                'data' => getConst('emptyData'),
                'show' => true
            ], 422);   
        }
        User::where('id', Auth::user()->id)->update(["total_monthly_income"=> $request->current_monthly_salary]);
        $returnData = $this->getEligibileLoanAmount(Auth::user()->id);
        if($returnData['is_eligibile'] == 1){
            return response()->json([
                'success' => true,
                'message' => "You are Eligible to Apply a Loan of Amount Upto ".$returnData['amountEligibileMax'],
                'data'    => [
                    'loanData' => $returnData,
                ],
                'show' => false
            ], 200);
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => "You are not Eligible to Apply a Loan",
                'data'    => getConst('emptyData'),
                'show' => true,
            ], 200);
        }
        } catch (\Exception $e) {
            CreateApiLogs($e->getMessage());
            Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => "Error! Something Went Wrong.",
                'data'    => getConst('emptyData'),
                'show' => true,
                'errorInfo' => $e->getMessage(),
                'error_code' => getConst('apiErrorCode.SERVER_ERROR.shortkey'),
            ], 400);
        }
    }

    public function applyForLoan(Request $request) {
        try {
        $validator = Validator::make($request->all(), [
            'applied_loan_amount' => 'required|integer|min:10000|max:9999999',
            'repayment_frequency' => 'required|in:'.implode(',',getConst('repaymentFrequencyType')),
            'emi_count' => 'required|integer|min:3|max:100',
            'check_breakpoints' => 'required|integer|min:0|max:1', // 0-direct applyForLoan |1- check Breakpoints
            'loan_type_id' => 'required|integer|exists:App\Models\LoanType,id',
        ]);
        if($validator->fails()) {  
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
                'data'    => getConst('emptyData'),
                'show' => true
            ], 422);   
        }

        $emiAmount = round($request->applied_loan_amount/$request->emi_count);

        if($emiAmount < 500) {  
            return response()->json([
                'success' => false,
                'message' => "Please Select EMI Amount Higher than ".$emiAmount,
                'data'    => getConst('emptyData'),
                'show' => true
            ], 200);   
        }


        $returnData = $this->getEligibileLoanAmount(Auth::user()->id);
        if($returnData['is_eligibile'] != 1){
            return response()->json([
                'success' => false,
                'message' => "You are not Eligible to Apply a Loan",
                'data'    => getConst('emptyData'),
                'show' => true,
            ], 200);
        }

        $breakpointArray = $this->getLoanBreakpoints($request->applied_loan_amount, $request->emi_count, $request->repayment_frequency, $request->loan_type_id);
        if($request->check_breakpoints == 1) {  
            return response()->json([
                'success' => true,
                'message' => "",
                'data'    => [
                    "data" => $breakpointArray,
                ],
                'show' => true
            ], 200);   
        }


        $dataInsert = [
            "user_id" => Auth::user()->id,
            "loan_type_id" => $request->loan_type_id,
            "amount" => $breakpointArray['total_amount'],
            "is_active" => getConst('is_active.true'),
            "processing_fee" => $breakpointArray['processing_fees'],
            "grand_amount" => $request->applied_loan_amount,
            "emi_count" => $request->emi_count,
            "repayment_frequency" => $request->repayment_frequency,
            "penalty_amount_total" => 0,
            "collected_amount_total" => 0,
            "tax_amount" => $breakpointArray['tax_fees'],
            "loan_percent" => $breakpointArray['loan_percent'],
        ];

        //dde($dataInsert);

        $lastCreated = Loan::create($dataInsert);

        if(isset($lastCreated->id)){
            for ($i=0; $i < $request->emi_count; $i++) { 

                if($request->repayment_frequency=="M"){
                    $start_date = Carbon::tomorrow()->addMonths($i+1)->format('Y-m-d');
                    $end_date = Carbon::tomorrow()->addMonths($i+1)->format('Y-m-d');
                } else {
                    $start_date = Carbon::tomorrow()->addWeeks($i+1)->format('Y-m-d');
                    $end_date = Carbon::tomorrow()->addWeeks($i+1)->format('Y-m-d');
                }
                LoanSegment::create([
                    "loan_id" => $lastCreated->id,
                    "is_active" => getConst('is_active.true'),
                    "emi_amount" => $breakpointArray['loanBreakpoints'][$i]['emiAmount'],
                    "emi_number" => $breakpointArray['loanBreakpoints'][$i]['emiNumber'],
                    "start_date" => $start_date,
                    "end_date" => $end_date,
                    "loan_percent" => $breakpointArray['loan_percent'],
                    "penalty_amount" => 0
                ]);
            }

            User::find(Auth::user()->id)->increment('wallet_balance', $breakpointArray['total_amount']);

            event(new LoanProcessed($lastCreated->toArray()));

            return response()->json([
                'success' => true,
                'message' => "Loan Processed Successfully.",
                'data'    => [
                    'first' => $lastCreated,
                ],
                'show' => true
            ], 200);
        }
        else
        {
            return response()->json([
                'success' => false,
                'message' => "Something Went Wrong. Please Try Again.",
                'data'    => getConst('emptyData'),
                'show' => true,
            ], 200);
        }
        } catch (\Exception $e) {
            CreateApiLogs($e->getMessage());
            Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => "Error! Something Went Wrong.",
                'data'    => getConst('emptyData'),
                'show' => true,
                'errorInfo' => $e->getMessage(),
                'error_code' => getConst('apiErrorCode.SERVER_ERROR.shortkey'),
            ], 400);
        }
    }


    public function listing(Request $request) {
        try {
        $searchTerm = ''; 
        $per_page = 50;  
        $sort = 'DESC';
        $is_active = '';
        $user_id = Auth::user()->id;
        if(isset($request->searchTerm)) {
            $searchTerm = RemoveSpecialChar($request->searchTerm);
        }
        if(isset($request->per_page)) {
            $per_page = intval($request->per_page);
        }
        if(isset($request->is_active)) {
            $is_active = intval($request->is_active);
        }
        if(isset($request->sort) && $request->sort=='ASC') {
            $sort =  'ASC';
        }

        
        $listing = Loan::where('user_id', $user_id) 
        // ->where('is_active', getConst('is_active.true'))
        ->where(function($queryActive) use ($is_active) {
            if($is_active!=''){
                $queryActive->where('is_active', $is_active);
            }
        })
        ->where(function($querySearch) use ($searchTerm) {
            if($searchTerm!=''){
                $querySearch->where('loan_number','LIKE', '%'.$searchTerm.'%');
            }
        })       
        ->orderBy('id','DESC')
        ->paginate($per_page);

        return response()->json([
            'success' => true,
            'message' => "",
            'data'    => [
                'listing' => $listing,
                'searchTerm' => $searchTerm,
            ],
            'show' => false,
        ], 200);

        } catch (\Exception $e) {
            CreateApiLogs($e->getMessage());
            Log::error($e->getMessage());
            return response()->json([
                'success' => false,
                'message' => "Error! Something Went Wrong.",
                'data'    => getConst('emptyData'),
                'show' => true,
                'errorInfo' => $e->getMessage(),
                'error_code' => getConst('apiErrorCode.SERVER_ERROR.shortkey'),
            ], 400);
        }
    }




}
