<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Loan;
use App\Models\LoanSegment;


class LoanHandleCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'loandeducthandle:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'loan deduct handle';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $loans = LoanSegment::with('loan')->where('start_date', date('Y-m-d'))->get();

        LoanSegment::with('loan')->where('start_date', date('Y-m-d'))->chunk(200, function ($loans) {
            foreach ($loans as $row) {
                User::find($row->loan->user_id)->decrement('wallet_balance', $row->emi_amount);
                LoanSegment::find($row->id)->update(["is_active" => 0]);
                Loan::find($row->loan->id)->increment('collected_amount_total', $row->emi_amount);
                $count = LoanSegment::where('is_active', 1)->where('loan_id', $row->loan->id)->count();
                if($count == 0){
                    Loan::find($row->loan->id)->update(["is_active" => 0]);
                }
            }
        });
        return true;
    }
}
